import QtQuick 2.6
import QtQuick.Dialogs 1.1
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

import org.kde.kcm 1.2
import org.kde.kirigami 2.4  as Kirigami // for Kirigami.Units
import org.kde.kcoreaddons 1.0 as KCoreAddons
import org.kde.userng 1.0

import QtQuick.Dialogs 1.2

ScrollViewKCM {
    id: root

    title: i18n("Manage users")

    LayoutMirroring.enabled: Qt.application.layoutDirection === Qt.RightToLeft
    LayoutMirroring.childrenInherit: true

    view: ListView {
        id: userList
        property int idx: -1;
        model: UserModel{}

        delegate: Kirigami.BasicListItem {
            width: userList.width
            icon: face
            iconColor: "transparent"
            label: name
            highlighted: index == userList.idx

            onClicked: {
                userList.idx = index
                kcm.push("UserDetailsPage.qml", {user: User})
            }
        }
    }

    footer: RowLayout {

        Item {
            Layout.alignment: Qt.AlignLeft
            Layout.fillWidth: true
        }

        Button {
            id: removeConnectionButton
            Layout.alignment: Qt.AlignRight

            enabled: userList.idx != -1

            icon.name: "list-remove"

            ToolTip.text: i18n("Remove selected user")
            ToolTip.visible: removeConnectionButton.hovered

            onClicked: {
                controller.deleteUser(userDetails.user.uid);
            }
        }

        Button {
            id: addConnectionButton
            Layout.alignment: Qt.AlignRight

            icon.name: "list-add"

            ToolTip.text: i18n("Add new user")
            ToolTip.visible: addConnectionButton.hovered

            onClicked: {
                dialog.open()
            }
        }
    }

    Component.onCompleted: {
        kcm.columnWidth = Kirigami.Units.gridUnit * 18
    }

    Dialog {
        id: dialog
        title: i18n("Create user")
        standardButtons: StandardButton.Save | StandardButton.Cancel

        Kirigami.FormLayout {
            anchors.centerIn: parent
            TextField {
                id: nameField
                Kirigami.FormData.label: i18n("Name:")
            }
//             TextField {
//                 echoMode: TextInput.Password
//                 Kirigami.FormData.label: i18n("Password:")
//             }
//             TextField {
//                 echoMode: TextInput.Password
//                 Kirigami.FormData.label: i18n("Confirm password:")
//             }
        }

        onAccepted: {
            controller.createUser(nameField.text)
        }
    }

    UserController {
        id: controller
    }

}
