import QtQuick 2.6
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5
import QtQuick.Dialogs 1.3

import org.kde.kcm 1.2
import org.kde.kirigami 2.4 as Kirigami
import org.kde.userng 1.0

SimpleKCM {
    id: connectionEditorPage

    title: "User details"

    property variant user;

    Connections {
        target: kcm

        onApply: {
            console.log("apply")
            user.realName = realNametextField.text
            user.email = emailTextField.text
            user.administrator = adminCheckBox.checked
            user.apply()
        }
    }

    Menu {
        id: imagePickerMenu

        MenuItem {
            text: i18n("Choose from file")
            icon.name: "folder-open-symbolic"
            onClicked: fileDialog.open()
        }

        MenuItem {
            text: i18n("Choose from gallery")
            icon.name: "shape-choose"
        }

        MenuItem {
            text: i18n("Take photo")
            icon.name: "camera-photo-symbol"
            onClicked: kcPicChooser.requestPicture()
        }
    }

    FileDialog {
        id: fileDialog
        title: "Choose a picture"
        onAccepted: {
            kcm.needsSave = true
            user.face = fileDialog.fileUrl
        }
    }

    KdeConnectPictureChooser {
        id: kcPicChooser
        onPictureAvailable: {
            console.log(fileName)
            kcm.needsSave = true
            user.face = "file://" + fileName
        }

    }

    ColumnLayout {

        Button {
            property int size: 6 * Kirigami.Units.gridUnit

            icon.source: user.face
            icon.width: size
            icon.height: size

            Layout.alignment: Qt.AlignHCenter
            Layout.maximumHeight: size + Kirigami.smallSpacing
            Layout.minimumHeight: size + Kirigami.smallSpacing
            Layout.maximumWidth: size + Kirigami.smallSpacing
            Layout.minimumWidth: size + Kirigami.smallSpacing

            onClicked: imagePickerMenu.popup()
        }
        Kirigami.FormLayout {

            TextField  {
                id: realNametextField
                focus: true
                text: user.realName
                placeholderText: i18n("John Doe")
                Kirigami.FormData.label: i18n("Name:")

                onTextChanged: {
                    kcm.needsSave = true
                }
            }

            TextField  {
                id: emailTextField
                focus: true
                text: user.email
                placeholderText: i18n("john.doe@kde.org")
                Kirigami.FormData.label: i18n("Email address:")

                onTextChanged: {
                    kcm.needsSave = true
                }
            }

            TextField  {
                focus: true
                Kirigami.FormData.label: i18n("Password:")
            }

            CheckBox {
                id: adminCheckBox
                checked: user.administrator
                text: i18n("Enable administrator privileges")

                onClicked: {
                    kcm.needsSave = true
                }
            }

//             CheckBox {
//                 id: autoLoginCheckbox
//                 checked: user.autologin
//                 text: i18n("Log in automatically")
//
//                 onClicked: {
//                     kcm.needsSave = true
//                 }
//             }
        }
    }
}
