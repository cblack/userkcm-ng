A user management config module for Plasma based on QML and Kirigami. Serves as a graphical interface for the Freedesktop.org AccountsService service.

![alt text](screenshots/initial.png)
![alt text](screenshots/details.png)