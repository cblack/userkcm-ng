// This file has been generated with qpropgen, any changes made to it will be lost!
#include <user.h>
#include <QDebug>
#include "user_interface.h"

User::User(QObject* parent)
    : QObject(parent) {
}

int User::uid() const {
    return mUid;
}
void User::setUid(int value) {
    
    if (mUid == value) {
        return;
    }
    mUid = value;
    uidChanged(value);
}

QString User::name() const {
    return mName;
}
void User::setName(const QString& value) {
    
    if (mName == value) {
        return;
    }
    mName = value;
    nameChanged(value);
}

QString User::realName() const {
    return mRealName;
}
void User::setRealName(const QString& value) {

    if (mRealName == value) {
        return;
    }
    mRealName = value;
    realNameChanged(value);
}

QString User::email() const {
    return mEmail;
}
void User::setEmail(const QString& value) {
    
    if (mEmail == value) {
        return;
    }
    mEmail = value;
    emailChanged(value);
}

QString User::face() const {
    return mFace;
}
void User::setFace(const QString& value) {

    QString face = QUrl(value).toLocalFile();

    if (mFace == face) {
        return;
    }
    mFace = face;
    faceChanged(face);
}

bool User::autologin() const {
    return mAutologin;
}
void User::setAutologin(bool value) {
    
    if (mAutologin == value) {
        return;
    }
    mAutologin = value;
    autologinChanged(value);
}

bool User::administrator() const {
    return mAdministrator;
}
void User::setAdministrator(bool value) {
    
    if (mAdministrator == value) {
        return;
    }
    mAdministrator = value;
    administratorChanged(value);
}

void User::setPath(const QDBusObjectPath &path) {
    m_dbusIface = new OrgFreedesktopAccountsUserInterface(QStringLiteral("org.freedesktop.Accounts"), path.path(), QDBusConnection::systemBus(), this);

    if (!m_dbusIface->isValid() || m_dbusIface->lastError().isValid() || m_dbusIface->systemAccount()) {
        return;
    }

    mPath = path;
    mUid = m_dbusIface->uid();
    mName = m_dbusIface->userName();
    mFace = m_dbusIface->iconFile();
    mRealName = m_dbusIface->realName();
    mEmail = m_dbusIface->email();
    mAdministrator = m_dbusIface->accountType() == 1;
    mAutologin = m_dbusIface->automaticLogin();
}

QDBusObjectPath User::path() const
{
    return mPath;
}


void User::apply()
{
    m_dbusIface->SetEmail(mEmail).waitForFinished();
    m_dbusIface->SetRealName(mRealName).waitForFinished();
    m_dbusIface->SetIconFile(mFace).waitForFinished();
    m_dbusIface->SetAccountType(mAdministrator ? 1 : 0).waitForFinished();
}

