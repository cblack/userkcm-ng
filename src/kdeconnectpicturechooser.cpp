#include "kdeconnectpicturechooser.h"
#include <QDBusMessage>
#include <QDBusConnection>
#include <QDebug>

KdeConnectPictureChooser::KdeConnectPictureChooser()
    : QObject()
{
    QDBusConnection::sessionBus().connect(QStringLiteral("org.kde.kdeconnectd"), QStringLiteral("/modules/kdeconnect/devices/b2a0243b1f93191c/photo"), QStringLiteral("org.kde.kdeconnect.device.photo"), QStringLiteral("photoReceived"), this, SLOT(rec(const QString&)));

}

void KdeConnectPictureChooser::requestPicture()
{
    QDBusMessage msg = QDBusMessage::createMethodCall(QStringLiteral("org.kde.kdeconnect"), QStringLiteral("/modules/kdeconnect/devices/b2a0243b1f93191c/photo"), QStringLiteral("org.kde.kdeconnect.device.photo"), "requestPhoto");
    msg.setArguments({"/home/nico/foo.jpg"});

    QDBusConnection::sessionBus().send(msg);
}

void KdeConnectPictureChooser::rec(const QString& fileName)
{
    Q_EMIT pictureAvailable(fileName);
}
